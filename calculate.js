function clickedNumber(number) {
    const display = document.getElementById("display");
    display.value = display.value + number;
}

function calculate() {
    const display = document.getElementById("display");
    const answer = document.getElementById("answer");
    const {value} = display;
    // const value = display.value;
    try {
        answer.innerHTML = eval(value)
    } catch(err){
        console.log(err)
        alert("Invalid expression please enter a valid expression")
    }
}